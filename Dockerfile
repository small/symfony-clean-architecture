FROM php:8.2

RUN usermod -u 1000 www-data

RUN pecl install openswoole && docker-php-ext-enable openswoole
RUN docker-php-ext-install pdo_mysql

# install git
RUN apt-get update && apt-get install -y git

# install pcov
RUN pecl install pcov && docker-php-ext-enable pcov

# Install zip
RUN apt-get update && apt-get -y install libzip-dev
RUN docker-php-ext-install zip

# Install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php --install-dir=/usr/bin --filename=composer

# Install gd for coverage badge
RUN apt-get update && apt-get install -y zlib1g-dev libpng-dev libfreetype-dev fonts-arkpandora
RUN docker-php-ext-configure gd --with-freetype
RUN docker-php-ext-install gd

RUN mkdir /usr/src/symfony-clean-architecture
RUN chown www-data:www-data /usr/src/symfony-clean-architecture
RUN chown www-data:www-data /var/www
USER www-data
WORKDIR /usr/src/symfony-clean-architecture

ENTRYPOINT sleep infinity