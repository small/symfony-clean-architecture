<?php

namespace Infrastructure\Encryption;

use Infrastructure\Encryption\Exception\EncryptionException;

class OpenSslRsaKey
{

    public function __construct(
        readonly protected string $privateKey,
        protected string|null $passphrase = null,
    ) {}

    public function getPrivateKey(): string
    {

        return $this->privateKey;

    }

    public function getPublicKey(): string
    {

        $publicKey = openssl_pkey_get_public(
            openssl_pkey_get_details(
                openssl_pkey_get_private($this->getPrivateKey(), $this->passphrase)
            )['key'],
        );

        $publicDetails = openssl_pkey_get_details($publicKey);
        if (!array_key_exists('key', $publicDetails)) {
            throw new EncryptionException('Can\t export public key');
        }

        return $publicDetails['key'];

    }

    public function getPassphrase(): string|null
    {

        return $this->passphrase;

    }

    public function encrypt(string $message): string
    {

        $key = openssl_pkey_get_private($this->getPrivateKey(), $this->passphrase);

        if (!openssl_private_encrypt($message, $messageEncrypted, $key)) {
            throw new EncryptionException('Encryption fail !');
        }

        return base64_encode($messageEncrypted);

    }

    public function decrypt(string $messageEncrypted): string
    {

        $key = openssl_pkey_get_private($this->getPrivateKey(), $this->passphrase);
        if (!openssl_private_decrypt( base64_decode( $messageEncrypted ), $message, $key)) {
            throw new EncryptionException('Decrypt fail !');
        }

        return $message;

    }

    public static function createKey(string $passphase = null, int $bits = 2048): self
    {

        openssl_pkey_export(openssl_pkey_new([
            "private_key_bits" => $bits,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
            "encrypt_key"        => true,
            "encrypt_key_cipher" => OPENSSL_CIPHER_AES_256_CBC,
        ]), $privateOut, $passphase);

        return new self($privateOut, $passphase);

    }


}