<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\SecurityBundle\SecurityBundle::class => ['all' => true],
    OpenSwooleBundle\Bridge\Symfony\Bundle\SwooleBundle::class => ['all' => true],
    Sebk\SmallOrmBundle\SebkSmallOrmBundle::class => ['all' => true],
    Infrastructure\Bundle\ApplicationModelBundle\ApplicationModelBundle::class => ['all' => true],
    Infrastructure\Bundle\CleanArchitectureMakerBundle\MakerBundle::class => ['all' => true],
    Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle::class => ['all' => true],
    Nelmio\CorsBundle\NelmioCorsBundle::class => ['all' => true],
];
