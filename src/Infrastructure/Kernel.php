<?php

namespace Infrastructure;

use Infrastructure\Encryption\OpenSslRsaKey;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    private const SECRET_DIR_PATH = __DIR__ . '/../../var/secret';
    private const APPLICATION_PRIVATE_KEY_PATH = self::SECRET_DIR_PATH . '/application.key';
    private const APPLICATION_PUBLIC_KEY_PATH = self::SECRET_DIR_PATH . '/application.pub.key';
    public readonly OpenSslRsaKey $applicationKey;

    public function __construct(string $environment, bool $debug)
    {

        // Generate application private key if not already exists
        if (!file_exists(self::APPLICATION_PRIVATE_KEY_PATH)) {
            $this->applicationKey = OpenSslRsaKey::createKey();
            !is_dir(self::SECRET_DIR_PATH)
                && mkdir(self::SECRET_DIR_PATH, 0700, true)
            ;
            file_put_contents(self::APPLICATION_PRIVATE_KEY_PATH, $this->applicationKey->getPrivateKey());
            chmod(self::APPLICATION_PRIVATE_KEY_PATH, 0600);
            file_put_contents(self::APPLICATION_PUBLIC_KEY_PATH, $this->applicationKey->getPublicKey());
            chmod(self::APPLICATION_PUBLIC_KEY_PATH, 0600);

        } else {
            $this->applicationKey = new OpenSslRsaKey(self::APPLICATION_PRIVATE_KEY_PATH);
        }
        
        parent::__construct($environment, $debug);

    }
}
