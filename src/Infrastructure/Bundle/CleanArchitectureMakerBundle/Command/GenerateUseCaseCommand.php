<?php

namespace Infrastructure\Bundle\CleanArchitectureMakerBundle\Command;

use Infrastructure\Bundle\CleanArchitectureMakerBundle\Generator\UseCaseGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateUseCaseCommand extends Command
{

    public function __construct(
        protected UseCaseGenerator $useCaseGenerator,
    ) {

        parent::__construct('clean-architecture-maker:generate:use-case');
        $this->setAliases(['mk:use-case']);

    }

    public function configure()
    {

        $this
            ->addArgument('name', InputArgument::REQUIRED, 'Name of use case at format : name/space/path/name')
        ;

    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {

        $this->useCaseGenerator->generate($input->getArgument('name'));

        return self::SUCCESS;

    }

}