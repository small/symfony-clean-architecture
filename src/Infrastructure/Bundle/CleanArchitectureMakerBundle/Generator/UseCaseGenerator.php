<?php

namespace Infrastructure\Bundle\CleanArchitectureMakerBundle\Generator;

use Domain\Application;
use ProxyManager\FileLocator\FileLocatorInterface;
use SmallClassManipulator\ClassFile\ClassFile;
use SmallClassManipulator\ClassFile\Element\BaseElement;
use SmallClassManipulator\ClassFile\Element\Bean\MethodStructure;
use SmallClassManipulator\ClassFile\Element\Bean\TypedVarStructure;
use SmallClassManipulator\ClassFile\Element\Enum\ClassScope;
use SmallClassManipulator\ClassFile\Element\MethodElement;
use SmallClassManipulator\ClassFile\InterfaceFile;
use SmallClassManipulator\ClassManipulator;
use SmallClassManipulator\Configuration\Configuration;

class UseCaseGenerator
{

    public function generate(string $useCaseName): self
    {

        // Check required paths
        Application::checkRequirePath();

        // Build base use case path
        $pathElements = explode('/', $useCaseName);
        $basePath = '';
        $i = 1;
        do {
            $basePath = $basePath . '/' . $pathElements[$i - 1];
            $i++;
        } while($i < count($pathElements));

        // Create directories if not exists
        $gatewayRequestPath = Application::BASE_PATH_INTERFACE_GATEWAY . '/Request' . $basePath;
        !is_dir($gatewayRequestPath) && mkdir($gatewayRequestPath, 0755, true);
        $gatewayResponsePath = Application::BASE_PATH_INTERFACE_GATEWAY . '/Response' . $basePath;
        !is_dir($gatewayResponsePath) && mkdir($gatewayResponsePath, 0755, true);
        $gatewayUseCasePath = Application::BASE_PATH_INTERFACE_GATEWAY . '/UseCase' . $basePath;
        !is_dir($gatewayUseCasePath) && mkdir($gatewayUseCasePath, 0755, true);
        $requestPath = Application::BASE_PATH_REQUEST . $basePath;
        !is_dir($requestPath) && mkdir($requestPath, 0755, true);
        $responsePath = Application::BASE_PATH_RESPONSE . $basePath;
        !is_dir($responsePath) && mkdir($responsePath, 0755, true);
        $useCasePath = Application::BASE_PATH_USE_CASE . $basePath;
        !is_dir($useCasePath) && mkdir($useCasePath, 0755, true);

        // Create class manipulator
        $manipulator = $this->getClassManipulator();

        // Create request interface
        $requestInterface = $this->createRequestInterface(
            $manipulator,
            $useCaseName
        );

        // Create response interface
        $responseInterface = $this->createResponseInterface(
            $manipulator,
            $useCaseName,
        );

        // Create use case interface
        $useCaseInterface = $this->createUseCaseInterface(
            $manipulator,
            $requestInterface,
            $responseInterface,
            $useCaseName,
        );

        // Create request class
        $this->createRequestClass(
            $manipulator,
            $requestInterface,
            $useCaseName,
        );

        // Create response class
        $responseClass = $this->createResponseClass(
            $manipulator,
            $responseInterface,
            $useCaseName,
        );

        // Create use case
        $this->createUseCaseClass(
            $manipulator,
            $requestInterface,
            $responseInterface,
            $useCaseInterface,
            $responseClass,
            $useCaseName,
        );

        return $this;

    }

    protected function getClassManipulator(): ClassManipulator
    {

        return new ClassManipulator([
            Configuration::ROOT_DIR => Application::BASE_DIR,
            Configuration::SELECTORS => [
                'gateway' => [
                    [
                        'namespace' => 'Domain\InterfaceAdapter\Gateway',
                        'path' => 'InterfaceAdapter/Gateway',
                    ],
                ], 'request' => [
                    [
                        'namespace' => 'Domain\Application\Request',
                        'path' => 'Application/Request',
                    ],
                ], 'response' => [
                    [
                        'namespace' => 'Domain\Application\Response',
                        'path' => 'Application/Response',
                    ],
                ], 'useCase' => [
                    [
                        'namespace' => 'Domain\Application\UseCase',
                        'path' => 'Application/UseCase',
                    ],
                ]
            ],
        ]);

    }

    private function createRequestInterface(
        ClassManipulator $manipulator,
        string $useCaseName,
    ): InterfaceFile {

        return $this->getClassManipulator()->newInterface(
            $this->getClassManipulator()
                ->getSelector('gateway')[0]
                ->getNamespace() . '\\Request\\' .
            str_replace('/', '\\', $useCaseName) . 'RequestInterface'
        )->generate($manipulator->getSelector('gateway'));

    }

    private function createResponseInterface(
        ClassManipulator $manipulator,
        string $useCaseName,
    ): InterfaceFile {

        return $this->getClassManipulator()->newInterface(
            $this->getClassManipulator()
                ->getSelector('gateway')[0]
                ->getNamespace() . '\\Response\\' .
            str_replace('/', '\\', $useCaseName) . 'ResponseInterface'
        )->generate($manipulator->getSelector('gateway'));

    }

    private function createUseCaseInterface(
        ClassManipulator $manipulator,
        InterfaceFile $requestInterface,
        InterfaceFile $responseInterface,
        string $useCaseName
    ): InterfaceFile {

        $useCaseInterface = $manipulator->newInterface(
            $manipulator
                ->getSelector('gateway')[0]
                ->getNamespace() . '\\UseCase\\' .
            str_replace('/', '\\', $useCaseName) . 'UseCaseInterface'
        );
        $useCaseInterface->setUses([
            (new BaseElement())
                ->setElement(
                    $requestInterface->getNamespace()->getElement() . '\\' .
                    $requestInterface->getInterfacename()->getElement()
                ),
            (new BaseElement())
                ->setElement(
                    $responseInterface->getNamespace()->getElement() . '\\' .
                    $responseInterface->getInterfacename()->getElement()
                ),
        ]);
        $useCaseInterface->getContentStructure()
            ->addMethod(
                (new MethodElement())
                    ->setElement(
                        $this->createExecuteStructure(
                            $requestInterface,
                            $responseInterface,
                        )
                    )
            );

        return $useCaseInterface->generate($manipulator->getSelector('gateway'));

    }

    private function createEmptyConstructor(): MethodElement
    {

        return (new MethodElement())
            ->setElement(
                (new MethodStructure(
                    '__construct',
                ))->setContent('{}')
            );

    }

    private function createExecuteStructure(
        InterfaceFile $requestInterface,
        InterfaceFile $responseInterface,
    ): MethodStructure {

        return new MethodStructure(
            'execute',
            [new TypedVarStructure(
                '$request',
                $requestInterface->getInterfacename()->getElement()
            )],
            '',
            ClassScope::public,
            false,
            $responseInterface->getInterfacename()->getElement(),
        );

    }

    private function createRequestClass(
        ClassManipulator $manipulator,
        InterfaceFile $requestInterface,
        string $useCaseName,
    ): ClassFile {

        $requestClass = $manipulator->newClass(
            $manipulator
                ->getSelector('request')[0]
                ->getNamespace() . '\\' .
            str_replace('/', '\\', $useCaseName) . 'Request'
        )->setUses([
            (new BaseElement())
                ->setElement(
                    $requestInterface->getNamespace()->getElement() . '\\' .
                    $requestInterface->getInterfacename()->getElement()
                )
        ])->setImplements(
            [
                (new BaseElement())
                    ->setElement(
                        $requestInterface->getInterfacename()->getElement()
                    )
            ]
        );
        $requestClass->getContentStructure()
            ->addMethod($this->createEmptyConstructor());
        return $requestClass->generate($manipulator->getSelector('request'));

    }

    private function createResponseClass(
        ClassManipulator $manipulator,
        InterfaceFile $responseInterface,
        string $useCaseName,
    ): ClassFile {

        $responseClass = $manipulator->newClass(
            $manipulator
                ->getSelector('response')[0]
                ->getNamespace() . '\\' .
            str_replace('/', '\\', $useCaseName) . 'Response'
        )->setUses([
            (new BaseElement())
                ->setElement(
                    $responseInterface->getNamespace()->getElement() . '\\' .
                    $responseInterface->getInterfacename()->getElement()
                )
        ])->setImplements(
            [
                (new BaseElement())
                    ->setElement(
                        $responseInterface->getInterfacename()->getElement()
                    )
            ]
        );
        $responseClass->getContentStructure()
            ->addMethod($this->createEmptyConstructor());

        return $responseClass->generate($manipulator->getSelector('response'));

    }

    private function createUseCaseClass(
        ClassManipulator $manipulator,
        InterfaceFile $requestInterface,
        InterfaceFile $responseInterface,
        InterfaceFile $useCaseInterface,
        ClassFile $responseClass,
        string $useCaseName,
    ): ClassFile {

        $useCase = ($manipulator->newClass(
            $manipulator
                ->getSelector('useCase')[0]
                ->getNamespace() . '\\' .
            str_replace('/', '\\', $useCaseName) . 'UseCase'
        ))->setUses(
            array_merge(
                $useCaseInterface->getUses(),
                [
                    (new BaseElement())
                        ->setElement(
                            $useCaseInterface->getNamespace()->getElement() . '\\' .
                            $useCaseInterface->getInterfacename()->getElement()
                        ),
                    (new BaseElement())
                        ->setElement(
                            $responseClass->getNamespace()->getElement() . '\\' .
                            $responseClass->getClassname()->getElement()
                        )
                ]
            )
        )->setImplements([$useCaseInterface->getInterfacename()]);
        $useCase->getContentStructure()
            ->addMethod(
                $this->createEmptyConstructor()
            )->addMethod(
                (new MethodElement())
                    ->setElement(
                        $this->createExecuteStructure($requestInterface, $responseInterface)
                            ->setContent(
                                "{\n\t\treturn new " .
                                $responseClass->getClassname()->getElement() . '()' .
                                ";\n\t}"
                            )
                    )
            );

        return $useCase->generate($manipulator->getSelector('useCase'));

    }

}