<?php

namespace Infrastructure\Bundle\ApplicationModelBundle\Dao;

use Sebk\SmallOrmCore\Dao\AbstractDao;

class User extends AbstractDao
{

    public function build()
    {

        $this->setDbTableName('user')
            ->setModelName('User')
            ->addPrimaryKey('id', 'id')
            ->addField('email', 'email')
            ->addField('nickname', 'nickname')
            ->addField('password', 'password')
            ->addField('salt', 'salt')
            ->addField('enabled', 'enabled')
            ->addField('role', 'role')
            ->addField('created_at', 'createdAt')
            ->addField('updated_at', 'UpdatedAt')
            ->addField('key', 'key')
        ;

    }

}