<?php

namespace Infrastructure\Bundle\ApplicationModelBundle\Validator;

use Sebk\SmallOrmCore\Validator\AbstractValidator;

class User extends AbstractValidator
{

    public function validate()
    {

        if (empty($this->model->getNickname())) {
            throw new \Exception('Nickname is required');
        }

        if (empty($this->model->getEmail())) {
            throw new \Exception('Email is required');
        }

        if (!$this->testUnique('nickname')) {
            throw new \Exception('Nickname must be unique');
        }

        if (!$this->testUnique('email')) {
            throw new \Exception('Email must be unique');
        }

        return true;

    }

}