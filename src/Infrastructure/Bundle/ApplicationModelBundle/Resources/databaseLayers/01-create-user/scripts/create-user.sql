CREATE TABLE `user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nickname` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `enabled` INT(1) NOT NULL DEFAULT 1,
  `password` VARCHAR(255) NOT NULL DEFAULT 1,
  `salt` VARCHAR(255) NOT NULL DEFAULT 1,
  `role` VARCHAR(255) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `key` TEXT NOT NULL,
PRIMARY KEY (`id`),
UNIQUE INDEX `nickname_UNIQUE` (`nickname` ASC) VISIBLE,
UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE);
