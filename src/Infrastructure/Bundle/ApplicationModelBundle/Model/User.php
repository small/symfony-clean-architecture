<?php

namespace Infrastructure\Bundle\ApplicationModelBundle\Model;

use Sebk\SmallOrmCore\Dao\Model;

/**
 * @method int getId()
 * @method string getEmail()
 * @method string getNickname()
 * @method bool getEnabled()
 * @method bool getPassword()
 * @method bool getSalt()
 * @method string getRole()
 * @method string getKey()
 * @method string getCreatedAt()
 * @method string getUpdatedAt()
 * @method self setId(int $id)
 * @method self setEmail(string $email)
 * @method self setNickname(string $nickname)
 * @method self setEnabled(bool $enabled)
 * @method self setPassword(string $password)
 * @method self setSalt(string $salt)
 * @method self setRole(string $role)
 * @method self setCreatedAt(string $createdAt)
 * @method self setUpdatedAt(string $updatedAt)
 * @method self setkey(string $key)
 */
class User extends Model
{

}