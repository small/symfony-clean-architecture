<?php

namespace Domain\Application\UseCase\Security;

use Domain\Application\Entity\Role;
use Domain\Application\Entity\User;
use Domain\Application\Enum\RoleType;
use Domain\Application\Response\Security\UpdateUserResponse;
use Domain\InterfaceAdapter\Gateway\Request\Security\UpdateUserRequestInterface;
use Domain\InterfaceAdapter\Gateway\Response\Security\UpdateUserResponseInterface;
use Domain\InterfaceAdapter\Gateway\UseCase\Security\UpdateUserUseCaseInterface;
use Domain\InterfaceAdapter\Manager\Security\UserProvider;

class UpdateUserUseCase implements UpdateUserUseCaseInterface
{

    public function __construct(
        protected UserProvider $userProvider
    ) {}

    /**
     * @param UpdateUserRequestInterface $request
     * @return UpdateUserResponseInterface
     * @throws \Exception
     */
    public function execute(UpdateUserRequestInterface $request): UpdateUserResponseInterface
    {

        $level = null;
        if ($request->getPassword() !== null) {
            $level = User::getPasswordLevel($request->getPassword());
        }

        $user = $this->userProvider->loadUserById($request->getId());

        if ($request->getNickname() !== null) {
            $user->setNickname($request->getNickname());
        }
        if ($request->getEmail() !== null) {
            $user->setEmail($request->getEmail());
        }
        if ($request->getRole() !== null) {
            $user->setRole(new Role(RoleType::from($request->getRole())));
        }
        if ($request->getEnabled() !== null) {
            $user->setEnabled($request->getEnabled());
        }

        $this->userProvider->updateUser(
            $user,
            $request->getPassword()
        );

        return new UpdateUserResponse($user, $level);

    }



}