<?php

namespace Domain\Application\UseCase\Security;

use Domain\Application\Response\Security\GetUserResponse;
use Domain\InterfaceAdapter\Adapter\Model\UserAdapter;
use Domain\InterfaceAdapter\Gateway\Request\Security\GetUserRequestInterface;
use Domain\InterfaceAdapter\Gateway\Response\Security\GetUserResponseInterface;
use Domain\InterfaceAdapter\Gateway\UseCase\Security\GetUserUseCaseInterface;
use Domain\InterfaceAdapter\Manager\Security\UserProvider;

class GetUserUseCase implements GetUserUseCaseInterface
{

    public function __construct(
        protected UserProvider $userProvider,
    ) {}

    public function execute(GetUserRequestInterface $request): GetUserResponseInterface
    {

        return new GetUserResponse(
            $this->userProvider->loadUserById($request->getUserId())
        );

    }

}