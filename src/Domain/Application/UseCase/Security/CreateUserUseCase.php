<?php

namespace Domain\Application\UseCase\Security;

use Domain\Application\Entity\Role;
use Domain\Application\Entity\User;
use Domain\Application\Enum\RoleType;
use Domain\Application\Response\Security\CreateUserResponse;
use Domain\InterfaceAdapter\Gateway\Request\Security\CreateUserRequestInterface;
use Domain\InterfaceAdapter\Gateway\Response\Security\CreateUserResponseInterface;
use Domain\InterfaceAdapter\Gateway\UseCase\Security\CreateUserUseCaseInterface;
use Domain\InterfaceAdapter\Manager\Security\UserProvider;

class CreateUserUseCase implements CreateUserUseCaseInterface
{

    public function __construct(
        protected UserProvider $userProvider,
    ) {}

    public function execute(CreateUserRequestInterface $request): CreateUserResponseInterface
    {

        $level = User::getPasswordLevel($request->getPassword());

        $user = (new User())
            ->setNickname($request->getNickname())
            ->setEmail($request->getEmail())
            ->setRole(new Role(RoleType::from($request->getRole())))
        ;
        $this->userProvider
            ->createUser($user, $request->getPassword())
        ;

        return new CreateUserResponse(
            $user,
            $level
        );

    }


}