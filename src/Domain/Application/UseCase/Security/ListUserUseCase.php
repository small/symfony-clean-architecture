<?php

namespace Domain\Application\UseCase\Security;

use Domain\Application\Response\Security\ListUserResponse;
use Domain\InterfaceAdapter\Gateway\Request\Security\ListUserRequestInterface;
use Domain\InterfaceAdapter\Gateway\UseCase\Security\ListUserUseCaseInterface;
use Domain\InterfaceAdapter\Manager\Security\UserProvider;

class ListUserUseCase implements ListUserUseCaseInterface
{

    public function __construct(
        protected UserProvider $userProvider,
    ) {}

    public function execute(ListUserRequestInterface $request): ListUserResponse
    {

        return new ListUserResponse($this->userProvider->listUsers());

    }


}