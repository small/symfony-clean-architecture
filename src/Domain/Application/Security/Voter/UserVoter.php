<?php

namespace Domain\Application\Security\Voter;

use Domain\Application\Entity\User;
use Domain\Application\Enum\ScopeType;
use Domain\Application\Request\Security\UpdateUserRequest;
use Sebk\SmallOrmForms\Form\AbstractForm;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter
{

    const CREATE = 'CREATE_USER';
    const UPDATE = 'UPDATE_USER';
    const LIST = 'LIST_USERS';

    protected function supports(string $attribute, $subject): bool
    {

        if (!in_array($attribute, [
            self::CREATE, self::UPDATE, self::LIST,
        ])) {
            return false;
        }

        return true;

    }

    /**
     * @param string $attribute
     * @param AbstractForm $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {

        /** @var User $user */
        $user = $token->getUser();

        switch ($attribute) {
            case self::CREATE:
            case self::LIST:
                if ($user->getRole()->getScopes()->has(ScopeType::userAdministration)) {
                    return true;
                }

                return false;
            case self::UPDATE:
                if (!$subject instanceof UpdateUserRequest) {
                    throw new \LogicException('Wrong request');
                }
                if ($user->getRole()->getScopes()->has(ScopeType::userAdministration)) {
                    if ($subject->getNickname() !== null) {
                        return false;
                    }
                    if ($subject->getEmail() !== null) {
                        return false;
                    }
                    if ($subject->getPassword()) {
                        return false;
                    }

                    return true;
                } else {
                    if ($user->getId() != $subject->getId()) {
                        return false;
                    }
                    if ($subject->getRole() !== null) {
                        return false;
                    }
                    if ($subject->getEnabled() !== null) {
                        return false;
                    }

                    return true;
                }

            default:
                throw new \LogicException('Unknown attribute to vote');
        }

    }


}