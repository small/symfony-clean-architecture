<?php

namespace Domain\Application\Request\Security;

use Domain\InterfaceAdapter\Gateway\Request\Security\ListUserRequestInterface;

readonly class ListUserRequest implements ListUserRequestInterface
{

}