<?php

namespace Domain\Application\Request\Security;

use Domain\InterfaceAdapter\Gateway\Request\Security\CreateUserRequestInterface;

readonly class CreateUserRequest implements CreateUserRequestInterface
{

    public function __construct(
        protected string $nickname,
        protected string $password,
        protected string $email,
        protected string $role,
    ) {}

    public function getNickname(): string
    {
        return $this->nickname;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getRole(): string
    {
        return $this->role;
    }

}