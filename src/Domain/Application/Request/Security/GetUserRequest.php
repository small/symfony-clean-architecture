<?php

namespace Domain\Application\Request\Security;

use Domain\InterfaceAdapter\Gateway\Request\Security\GetUserRequestInterface;

readonly class GetUserRequest implements GetUserRequestInterface
{

    public function __construct(
        protected int $userId,
    ) {}

    public function getUserId(): int
    {
        return $this->userId;
    }

}