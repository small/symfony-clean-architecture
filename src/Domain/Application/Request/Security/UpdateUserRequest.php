<?php

namespace Domain\Application\Request\Security;

use Domain\Application\Entity\User;
use Domain\InterfaceAdapter\Gateway\Request\Security\UpdateUserRequestInterface;

readonly class UpdateUserRequest implements UpdateUserRequestInterface
{

    public function __construct(
        protected int $id,
        protected string|null $nickname,
        protected string|null $email,
        protected string|null $role,
        protected string|null $password,
        protected bool|null $enabled,
    ) {}

    public function getId(): string|null
    {

        return $this->id;

    }

    public function getNickname(): string|null
    {

        return $this->nickname;

    }

    public function getEmail(): string|null
    {
        return $this->email;
    }

    public function getRole(): string|null
    {
        return $this->role;
    }

    public function getPassword(): string|null
    {
        return $this->password;
    }

    public function getEnabled(): bool|null
    {
        return $this->enabled;
    }

}