<?php

namespace Domain\Application\Enum;

enum RoleType: string
{

    case user = 'user';
    case administrator = 'administrator';

}