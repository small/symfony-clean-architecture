<?php

namespace Domain\Application\Enum;

enum ScopeType: string
{

    case userAdministration = 'userAdministration';
    case basicUse = 'basic_use';

}
