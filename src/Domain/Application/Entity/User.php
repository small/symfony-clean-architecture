<?php

namespace Domain\Application\Entity;

use Domain\Application\Entity\Exception\WrongEmailFormatException;
use Domain\Application\Enum\RoleType;
use Domain\Entity\PasswordTooWeakException;
use Infrastructure\Encryption\OpenSslRsaKey;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface, EquatableInterface, PasswordAuthenticatedUserInterface
{

    const DEFAULT_PASSWORD = 'FIELD_NOT_PERSIST';

    protected int|null $id = null;
    protected string $email;
    protected string $nickname;
    protected bool $enabled = true;
    protected string $password;
    protected string $salt;
    protected \DateTime $createdAt;
    protected \DateTime|null $updatedAt;
    protected Role $role;
    protected OpenSslRsaKey|null $key = null;

    /**
     * Return user id
     * @return string
     */
    public function getId(): int|null
    {
        return $this->id;
    }

    public function getUserIdentifier(): int
    {

        return $this->id;

    }

    /**
     * Set user id
     * @return string
     */
    public function setId(int|null $id): self
    {

        $this->id = $id;

        return $this;

    }

    /**
     * Get username : alias of getEmail
     * @return string
     */
    public function getUsername(): string
    {
        return $this->email;
    }

    /**
     * Get email
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Get nickname
     * @return string
     */
    public function getNickname(): string
    {
        return $this->nickname;
    }

    /**
     * Get created at
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Get updated at
     * @return \DateTime
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Get roles as array
     * @return Role
     */
    public function getRole(): Role
    {
        return $this->role;
    }

    /**
     * Set email
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): User
    {

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new WrongEmailFormatException('Fail to validate email');
        }

        $this->email = $email;

        return $this;
    }

    /**
     * Set nickname
     * @param string $nickname
     * @return $this
     */
    public function setNickname(string $nickname): User
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Set enabled
     * @param bool $enabled
     * @return $this
     */
    public function setEnabled(bool $enabled): User
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Set created at
     * @param \DateTime|null $createdAt
     * @return User
     */
    public function setCreatedAt(?\DateTime $createdAt): User
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Set updated at
     * @param \DateTime|null $updatedAt
     * @return User
     */
    public function setUpdatedAt(?\DateTime $updatedAt): User
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Set role
     * @param array $role
     * @return $this
     */
    public function setRole(Role $role): User
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Return true if same user as this
     * @param UserInterface $user
     * @return bool
     */
    public function isEqualTo(UserInterface $user): bool
    {
        if(!$user instanceof User) {
            return false;
        }

        if($this->getUsername() != $user->getUsername()) {
            return false;
        }
        return true;
    }

    /**
     * Return true if user is enabled
     * @return bool
     */
    public function isEnabled():bool
    {
        return $this->enabled;
    }

    public static function getPasswordLevel(string $password): int
    {

        $level = 1;
        if (($length = strlen($password)) < 8) {
            throw new PasswordTooWeakException('The password must have at least 8 chars');
        }

        if ($length > 16) {
            $level++;
        }

        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number    = preg_match('@[0-9]@', $password);
        $specialChars = preg_match('@[^\w]@', $password);

        if (!$uppercase && !$lowercase) {
            throw new PasswordTooWeakException('The password must contains alphabetical chars');
        }

        if ($uppercase && $lowercase) {
            $level++;
        }

        if ($number && !$uppercase && !$lowercase) {
            throw new PasswordTooWeakException('The password must not be numerical');
        }

        if ($number) {
            $level++;
        }

        if ($specialChars) {
            $level++;
        }

        return $level;

    }

    /**
     * @param string $password
     * @return OpenSslRsaKey
     */
    public function createKey(string $password): OpenSslRsaKey
    {

        $this->password = $password;
        return $this->key = OpenSslRsaKey::createKey($password);

    }

    /**
     * @param string $key
     * @param string $password
     * @return OpenSslRsaKey
     */
    public function loadKey(string $key, string $password): OpenSslRsaKey
    {

        $this->password = $password;
        $this->key = new OpenSslRsaKey($key, $password);

        return $this->key;

    }

    public function getKey(): OpenSslRsaKey
    {

        return $this->key;

    }

    public function getPassword(): string
    {

        return $this->password;

    }

    public function setPassword(string $password): self
    {

        $this->password = $password;

        return $this;

    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->role->getScopes()->toArray();
    }

    public function getSalt(): string
    {

        return $this->salt;

    }

    public function setSalt(string $salt): self
    {

        $this->salt = $salt;

        return $this;

    }

    public function eraseCredentials()
    {

        $this->password = '';
        $this->salt = '';
        $this->key = null;

    }


}