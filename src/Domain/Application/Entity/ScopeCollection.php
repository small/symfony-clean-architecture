<?php

namespace Domain\Application\Entity;

use Domain\Application\Enum\ScopeType;
use Small\Collection\Collection;
use Small\Collection\Contract\CheckValueInterface;

/**
 * @method ScopeType offsetGet()
 * @method offsetSet(ScopeType $offset)
 */
class ScopeCollection
    extends Collection
    implements CheckValueInterface
{

    public function checkValue(mixed $value): static
    {

        if (!$value instanceof ScopeType) {
            throw new \TypeError(self::class . ' accept only ' . ScopeType::class . ' as value');
        }

        return $this;

    }

    public function has(ScopeType $scope): bool
    {

        /** @var ScopeType $value */
        foreach ($this->array as $value) {
            if ($scope == $value) {
                return true;
            }
        }

        return false;

    }

}