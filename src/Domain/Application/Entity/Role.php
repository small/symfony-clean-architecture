<?php

namespace Domain\Application\Entity;

use Domain\Application\Enum\RoleType;
use Domain\Application\Enum\ScopeType;

class Role
{

    protected const SCOPES = [
        RoleType::user->name => [ScopeType::basicUse],
        RoleType::administrator->name => [
            ScopeType::userAdministration,
            ScopeType::basicUse
        ],
    ];

    public function __construct(
        protected RoleType $role,
    ) {}

    public function getRole(): RoleType
    {

        return $this->role;

    }

    public function getScopes(): ScopeCollection
    {

        return new ScopeCollection(self::SCOPES[$this->role->name]);

    }

}