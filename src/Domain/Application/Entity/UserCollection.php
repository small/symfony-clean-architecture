<?php

namespace Domain\Application\Entity;

use Domain\Application\Entity\Exception\CollectionException;
use Small\Collection\Collection;
use Small\Collection\Contract\CheckValueInterface;

class UserCollection
    extends Collection
    implements CheckValueInterface
{

    public function checkValue(mixed $value): static
    {

        if (!$value instanceof User) {
            throw new CollectionException(static::class . ' accept only ' . User::class);
        }

        return $this;

    }

}