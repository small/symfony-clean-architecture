<?php

namespace Domain\Application\Response\Security;

use Domain\Application\Entity\User;
use Domain\InterfaceAdapter\Gateway\Response\Security\CreateUserResponseInterface;

readonly class CreateUserResponse implements CreateUserResponseInterface
{

    public function __construct(
        protected User $user,
        protected int|null $passwordStrengthLevel = null,
    ) {}

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return int|null
     */
    public function getPasswordStrengthLevel(): ?int
    {
        return $this->passwordStrengthLevel;
    }

}