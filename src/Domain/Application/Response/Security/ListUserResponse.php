<?php

namespace Domain\Application\Response\Security;

use Domain\Application\Entity\UserCollection;
use Domain\InterfaceAdapter\Gateway\Response\Security\ListUserResponseInterface;

readonly class ListUserResponse implements ListUserResponseInterface
{

    public function __construct(
        protected UserCollection $users
    ) {}

    public function getUsers(): UserCollection
    {

        return $this->users;

    }


}