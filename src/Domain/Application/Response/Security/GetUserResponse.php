<?php

namespace Domain\Application\Response\Security;

use Domain\Application\Entity\User;
use Domain\InterfaceAdapter\Gateway\Response\Security\GetUserResponseInterface;

readonly class GetUserResponse implements GetUserResponseInterface
{

    public function __construct(
        protected User $user,
    ) {}

    public function getUser(): User
    {
        return $this->user;
    }

}