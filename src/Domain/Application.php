<?php

namespace Domain;

class Application
{

    public const BASE_DIR = __DIR__;
    public const BASE_PATH_ENTITY = __DIR__ . '/Application/Entity';
    public const BASE_PATH_REQUEST = __DIR__ . '/Application/Request';
    public const BASE_PATH_RESPONSE = __DIR__ . '/Application/Response';
    public const BASE_PATH_USE_CASE = __DIR__ . '/Application/UseCase';
    public const BASE_PATH_INTERFACE_GATEWAY = __DIR__ . '/InterfaceAdapter/Gateway';

    /**
     * Check require directory structure
     * @return void
     * @throws \Exception
     */
    public static function checkRequirePath(): void
    {

        if (!is_dir(self::BASE_PATH_ENTITY)) {
            throw new \Exception('Missing entity dir (' . self::BASE_PATH_ENTITY . ')');
        }

        if (!is_dir(self::BASE_PATH_REQUEST)) {
            throw new \Exception('Missing request dir (' . self::BASE_PATH_REQUEST . ')');
        }

        if (!is_dir(self::BASE_PATH_RESPONSE)) {
            throw new \Exception('Missing response dir (' . self::BASE_PATH_RESPONSE . ')');
        }

        if (!is_dir(self::BASE_PATH_USE_CASE)) {
            throw new \Exception('Missing use case dir (' . self::BASE_PATH_USE_CASE . ')');
        }

    }

}