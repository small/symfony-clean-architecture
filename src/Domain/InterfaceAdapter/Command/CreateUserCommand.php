<?php

namespace Domain\InterfaceAdapter\Command;

use Domain\Application\Request\Security\CreateUserRequest;
use Domain\Application\UseCase\Security\CreateUserUseCase;
use Domain\InterfaceAdapter\Presenter\Console\UserToOutput;
use Domain\InterfaceAdapter\Presenter\Security\UserPresenter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:user:create',
    description: 'Creates a new user',
    hidden: false,
)]
class CreateUserCommand extends Command
{

    public function __construct(
        protected CreateUserUseCase $createUserUseCase,
    ) {
        parent::__construct('app:user:create');
    }

    public function configure(): void
    {
        $this->addArgument('nickname', InputArgument::REQUIRED);
        $this->addArgument('email', InputArgument::REQUIRED);
        $this->addArgument('password', InputArgument::REQUIRED);
        $this->addArgument('role', InputArgument::OPTIONAL, 'role (user, adminitrator', 'user');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $response = $this->createUserUseCase->execute(
            new CreateUserRequest(
                $input->getArgument('nickname'),
                $input->getArgument('password'),
                $input->getArgument('email'),
                $input->getArgument('role'),
            )
        );

        (new UserPresenter($response->getUser(), $response->getPasswordStrengthLevel()))
            ->toOutput($output)
        ;

        $output->writeln('** User created **');

        return self::SUCCESS;

    }

}