<?php

namespace Domain\InterfaceAdapter\Gateway\UseCase\Security;

use Domain\Application\Response\Security\ListUserResponse;
use Domain\InterfaceAdapter\Gateway\Request\Security\ListUserRequestInterface;

interface ListUserUseCaseInterface
{

    public function execute(ListUserRequestInterface $request): ListUserResponse;

}