<?php

namespace Domain\InterfaceAdapter\Gateway\UseCase\Security;

use Domain\InterfaceAdapter\Gateway\Request\Security\GetUserRequestInterface;
use Domain\InterfaceAdapter\Gateway\Response\Security\GetUserResponseInterface;

interface GetUserUseCaseInterface
{

    public function execute(GetUserRequestInterface $request): GetUserResponseInterface;

}