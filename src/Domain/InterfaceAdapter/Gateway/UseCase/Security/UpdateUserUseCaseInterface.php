<?php

namespace Domain\InterfaceAdapter\Gateway\UseCase\Security;

use Domain\InterfaceAdapter\Gateway\Request\Security\UpdateUserRequestInterface;
use Domain\InterfaceAdapter\Gateway\Response\Security\UpdateUserResponseInterface;

interface UpdateUserUseCaseInterface
{

    public function execute(UpdateUserRequestInterface $request): UpdateUserResponseInterface;

}