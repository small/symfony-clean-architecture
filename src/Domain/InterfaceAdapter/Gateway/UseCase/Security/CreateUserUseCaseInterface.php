<?php

namespace Domain\InterfaceAdapter\Gateway\UseCase\Security;

use Domain\InterfaceAdapter\Gateway\Request\Security\CreateUserRequestInterface;
use Domain\InterfaceAdapter\Gateway\Response\Security\CreateUserResponseInterface;

interface CreateUserUseCaseInterface
{

    public function execute(CreateUserRequestInterface $request): CreateUserResponseInterface;

}