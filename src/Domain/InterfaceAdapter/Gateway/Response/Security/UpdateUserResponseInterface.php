<?php

namespace Domain\InterfaceAdapter\Gateway\Response\Security;

use Domain\Application\Entity\User;

interface UpdateUserResponseInterface
{

    public function getUser(): User;

}