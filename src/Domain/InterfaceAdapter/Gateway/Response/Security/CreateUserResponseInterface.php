<?php

namespace Domain\InterfaceAdapter\Gateway\Response\Security;

use Domain\Application\Entity\User;

interface CreateUserResponseInterface
{

    public function getUser(): User;
    public function getPasswordStrengthLevel(): int|null;

}