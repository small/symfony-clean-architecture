<?php

namespace Domain\InterfaceAdapter\Gateway\Response\Security;

use Domain\Application\Entity\UserCollection;

interface ListUserResponseInterface
{

    public function getUsers(): UserCollection;

}