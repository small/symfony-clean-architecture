<?php

namespace Domain\InterfaceAdapter\Gateway\Response\Security;

use Domain\Application\Entity\User;

interface GetUserResponseInterface
{

    public function getUser(): User;

}