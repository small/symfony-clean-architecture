<?php

namespace Domain\InterfaceAdapter\Gateway\Request\Security;

interface GetUserRequestInterface
{

    public function getUserId(): int;

}