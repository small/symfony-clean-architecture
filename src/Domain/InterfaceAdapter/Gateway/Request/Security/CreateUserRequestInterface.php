<?php

namespace Domain\InterfaceAdapter\Gateway\Request\Security;

interface CreateUserRequestInterface
{

    public function getNickname(): string;

    public function getEmail(): string;
    public function getRole(): string;

    public function getPassword(): string;

}