<?php

namespace Domain\InterfaceAdapter\Gateway\Request\Security;

interface UpdateUserRequestInterface
{

    public function getId(): string|null;
    public function getNickname(): string|null;
    public function getEmail(): string|null;
    public function getRole(): string|null;
    public function getPassword(): string|null;
    public function getEnabled(): bool|null;

}