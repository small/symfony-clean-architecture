<?php

namespace Domain\InterfaceAdapter\Manager\Security;

use Domain\Application\Entity\User;
use Domain\Application\Entity\UserCollection;
use Domain\Application\UseCase\Security\CreateUserUseCase;
use Domain\Application\UseCase\Security\Exception\AccessDeniedException;
use Domain\Application\UseCase\Security\Exception\UserNotFoundException;
use Domain\InterfaceAdapter\Adapter\Entity\UserAdapter;
use Domain\Manager\Security\UnsupportedUserException;
use Domain\Manager\Security\UserInterface;
use Infrastructure\bundle\ApplicationModelBundle\Model\User as UserModel;
use Sebk\SmallOrmCore\Dao\DaoEmptyException;
use Sebk\SmallOrmCore\Dao\DaoException;
use Sebk\SmallOrmCore\Dao\Model;
use Sebk\SmallOrmCore\Factory\Dao;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Small\Collection\Collection;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{

    public function __construct(
        protected Dao $daoFactory,
        protected UserPasswordHasherInterface $passwordHasher,
    ) {}

    public function getUserDao(): \Infrastructure\Bundle\ApplicationModelBundle\Dao\User
    {

        return $this->daoFactory
            ->get("ApplicationModelBundle", "User")
        ;

    }

    /**
     * Load user by email or nickname
     * @param string $username
     * @return User
     * @throws DaoException
     * @throws \Sebk\SmallOrmCore\Factory\ConfigurationException
     * @throws \Sebk\SmallOrmCore\Factory\DaoNotFoundException
     */
    public function loadUserByUsername(string $username): User
    {

        // Get model from db
        $model = $this->getModelByUsername($username);

        // Return user entity
        return (new \Domain\InterfaceAdapter\Adapter\Model\UserAdapter($model))
            ->createEntity()
        ;

    }

    public function listUsers(): UserCollection
    {

        $userModels = new Collection($this->getUserDao()->findBy([]));

        return new UserCollection(
            $userModels->map(function (
                int $key,
                UserModel $userModel
            ): User {
                return (new \Domain\InterfaceAdapter\Adapter\Model\UserAdapter($userModel))->createEntity();
            })->toArray()
        );

    }

    /**
     * Load user by id
     * @param int $userId
     * @return User
     * @throws DaoException
     * @throws \Sebk\SmallOrmCore\Factory\ConfigurationException
     * @throws \Sebk\SmallOrmCore\Factory\DaoNotFoundException
     */
    public function loadUserById(int $userId): User
    {

        // Get model from db
        $model = $this->getModelById($userId);

        // Return user entity
        return (new \Domain\InterfaceAdapter\Adapter\Model\UserAdapter($model))->createEntity();

    }

    /**
     * Alias for loadUserById
     * @param int $userId
     * @return User
     * @throws DaoException
     * @throws \Sebk\SmallOrmCore\Factory\ConfigurationException
     * @throws \Sebk\SmallOrmCore\Factory\DaoNotFoundException
     */
    public function loadUserByIdentifier(string $username): User
    {

        return $this->loadUserByUsername($username);

    }

    /**
     * Get user model by email or nickname
     * @param string $username
     * @return UserModel
     * @throws DaoException
     * @throws \Sebk\SmallOrmCore\Factory\ConfigurationException
     * @throws \Sebk\SmallOrmCore\Factory\DaoNotFoundException
     */
    public function getModelByUsername(string $username): UserModel
    {

        try {
            $user = $this->getUserDao()->findOneBy(array("email" => $username));
        } catch (DaoEmptyException $e) {
            try {
                $user = $this->getUserDao()->findOneBy(array("nickname" => $username));
            } catch (DaoEmptyException $e) {
                throw new UserNotFoundException("User $username does not exist.");
            }
        }

        return $user;

    }

    /**
     * Get user model by id
     * @param int $userId
     * @return UserModel
     * @throws DaoException
     * @throws \Sebk\SmallOrmCore\Factory\ConfigurationException
     * @throws \Sebk\SmallOrmCore\Factory\DaoNotFoundException
     */
    public function getModelById(int $userId): UserModel
    {

        try {
            $user = $this->getUserDao()->findOneBy(["id" => $userId]);
        } catch (DaoEmptyException $e) {
            throw new UserNotFoundException("User id $userId does not exist.");
        }

        return $user;

    }

    /**
     * Refresh user
     * @param UserInterface $user
     * @return User
     * @throws DaoException
     * @throws \Sebk\SmallOrmCore\Factory\ConfigurationException
     * @throws \Sebk\SmallOrmCore\Factory\DaoNotFoundException
     */
    public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user): User
    {

        if (!$user instanceof User) {
            throw new UnsupportedUserException("Instances of " . $user::class . " are not supported.");
        }

        return $this->loadUserByUsername($user->getUsername());

    }

    /**
     * Is class is supported by provider
     * @param string $class
     * @return bool
     */
    public function supportsClass(string $class): bool
    {

        return User::class == $class;

    }

    /**
     * Create user
     * @param string $email
     * @param string $nickname
     * @param string $plainPassword
     * @param bool $enabled
     * @return $this
     * @throws \ReflectionException
     * @throws \Sebk\SmallOrmCore\Factory\ConfigurationException
     * @throws \Sebk\SmallOrmCore\Factory\DaoNotFoundException
     */
    public function createUser(User $userEntity, string $plainPassword): UserProvider
    {

        // Secrets
        $userEntity->setSalt($this->createSalt());
        $userEntity->setPassword($this->passwordHasher->hashPassword($userEntity, $plainPassword));
        $userEntity->createKey($userEntity->getPassword());

        $userEntity->setCreatedAt(new \DateTime());
        $userEntity->setUpdatedAt(new \DateTime());

        /** @var UserModel $userModel */
        $userModel = $this->getUserDao()->newModel();
        (new UserAdapter($userEntity))->hydrateModel($userModel);

        $userModel->getValidator()->validate();
        $userModel->persist();
        $userEntity->setId($userModel->getId());

        return $this;

    }


    /**
     * Update user
     * @param User $user
     * @param string|null $plainPassword
     * @return UserProvider
     * @throws \Exception
     */
    public function updateUser(User $userEntity, string $plainPassword = null): UserProvider
    {

        if ($userEntity->getId() === null) {
            throw new AccessDeniedException('Can\'t update user');
        }

        $userEntity->setUpdatedAt(new \DateTime());

        /** @var UserModel $model */
        $model = $this->getUserDao()->findOneBy(['id' => $userEntity->getId()]);

        if($plainPassword !== null) {
            $userEntity->setSalt($this->createSalt());
            $userEntity->setPassword($this->passwordHasher->hashPassword($userEntity, $plainPassword));
            $userEntity->createKey($userEntity->getPassword());
        }

        (new UserAdapter($userEntity))->hydrateModel($model);
        if ($plainPassword === null) {
            // Or not persist for security reasons
            ;
        }

        $model->getValidator()->validate();
        $model->persist();

        return $this;

    }

    private function createSalt(): string
    {

        return base64_encode(random_bytes(10));

    }

    /**
     * Update a user from model
     * @param UserModel $model
     * @param string|null $plainPassword
     * @return UserProvider
     * @throws \Exception
     */
    public function updateUserFromModel(UserModel $model, string $plainPassword = null): UserProvider
    {

        $user = new User;
        $user->setFromModel($model);

        return $this->updateUser($user, $plainPassword);

    }

    /**
     * Check if password match user password
     * @param User $user
     * @param string $plainPassword
     * @return bool
     * @throws DaoException
     * @throws \Sebk\SmallOrmCore\Factory\ConfigurationException
     * @throws \Sebk\SmallOrmCore\Factory\DaoNotFoundException
     */
    public function checkPassword(User $user, string $plainPassword): bool
    {

        echo "check";
        if (!$user->getEnabled()) {
            echo "test";
            return false;
        }

        $this->loadUserById($user->getId(), $plainPassword);

        return false;

    }

}