<?php

namespace Domain\InterfaceAdapter\Adapter\Entity;

use Domain\Application\Entity\User;
use Sebk\SmallOrmCore\Dao\Model;

class UserAdapter
{

    const MYSQL_DATETIME_FORMAT = 'Y-m-d H:i:s';

    public function __construct(
        protected User $entity
    ) {}

    public function hydrateModel(\Infrastructure\bundle\ApplicationModelBundle\Model\User &$model): self
    {

        $model->setId($this->entity->getId())
            ->setEmail($this->entity->getEmail())
            ->setNickname($this->entity->getNickname())
            ->setEnabled($this->entity->isEnabled() ? 1 : 0)
            ->setPassword($this->entity->getPassword())
            ->setSalt($this->entity->getSalt())
            ->setCreatedAt($this->entity->getCreatedAt()->format(self::MYSQL_DATETIME_FORMAT))
            ->setUpdatedAt($this->entity->getUpdatedAt()->format(self::MYSQL_DATETIME_FORMAT))
            ->setRole($this->entity->getRole()->getRole()->name)
            ->setkey($this->entity->getKey()->getPrivateKey());

        return $this;

    }

}