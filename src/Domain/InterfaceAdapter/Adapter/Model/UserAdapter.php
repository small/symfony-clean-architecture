<?php

namespace Domain\InterfaceAdapter\Adapter\Model;

use Domain\Application\Entity\Role;
use Domain\Application\Entity\User;
use Domain\Application\Enum\RoleType;

class UserAdapter
{

    public function __construct(
        protected \Infrastructure\bundle\ApplicationModelBundle\Model\User $model
    ) {}

    public function createEntity(): User
    {

        $entity = new User();
        $this->hydrateEntity($entity);

        return $entity;

    }

    public function hydrateEntity(User &$entity): self
    {

        $entity->setId($this->model->getId())
            ->setEmail($this->model->getEmail())
            ->setNickname($this->model->getNickname())
            ->setEnabled($this->model->getEnabled() == 1)
            ->setPassword($this->model->getPassword())
            ->setSalt($this->model->getSalt())
            ->setRole(new Role(RoleType::from($this->model->getRole())))
            ->setCreatedAt(
                \DateTime::createFromFormat(
                    $this->model::MYSQL_FORMAT_DATETIME,
                    $this->model->getCreatedAt()
                )
            )
            ->setUpdatedAt(
                $this->model->getUpdatedAt() == null
                    ? null
                    : \DateTime::createFromFormat(
                        $this->model::MYSQL_FORMAT_DATETIME,
                        $this->model->getUpdatedAt()
                    )
            )
            ->loadKey($this->model->getKey(), $this->model->getPassword())
        ;

        return $this;

    }

}