<?php

namespace Domain\InterfaceAdapter\Presenter\Security;

use Domain\Application\Entity\User;
use Domain\Application\Enum\ScopeType;
use Symfony\Component\Console\Output\Output;

readonly class UserPresenter
{

    public function __construct(
        protected User $user,
        protected int|null $passwordStrenghtLevel = null,
    ) {}

    public function toOutput(Output $output): self
    {

        $output->writeln('Nickname : ' . $this->user->getNickname());
        $output->writeln('Email : ' . $this->user->getEmail());
        $output->writeln('Role : ' . $this->user->getRole()->getRole()->value);
        if ($this->passwordStrenghtLevel !== null) {
            $output->writeln('Password strength level : ' . $this->passwordStrenghtLevel);
        }
        $output->writeln('-----------------------');

        return $this;

    }

    /**
     * @return mixed[]
     */
    public function toArray(): array
    {

        $response = [
            'id' => $this->user->getId(),
            'nickname' => $this->user->getNickname(),
            'email' => $this->user->getEmail(),
            'role' => $this->user->getRole()->getRole()->value,
            'createdAt' => $this->user->getCreatedAt()->format('d/m/y H:i:s'),
            'updatedAt' => $this->user->getUpdatedAt()->format('d/m/y H:i:s'),
            'scopes' => $this->user->getRole()->getScopes()->map(function (int $key, ScopeType $scope) {
                return $scope->value;
            }),
            'enabled' => $this->user->isEnabled(),
        ];

        if ($this->passwordStrenghtLevel !== null) {
            $response['passwordStrenghtLevel'] = $this->passwordStrenghtLevel;
        }

        return $response;

    }

}