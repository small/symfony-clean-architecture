<?php

namespace Domain\InterfaceAdapter\Controller;

use Domain\Application\Security\Voter\UserVoter;
use Domain\Application\Request\Security\CreateUserRequest;
use Domain\Application\Request\Security\GetUserRequest;
use Domain\Application\Request\Security\ListUserRequest;
use Domain\Application\Request\Security\UpdateUserRequest;
use Domain\Application\UseCase\Security\CreateUserUseCase;
use Domain\Application\UseCase\Security\GetUserUseCase;
use Domain\Application\UseCase\Security\ListUserUseCase;
use Domain\Application\UseCase\Security\UpdateUserUseCase;
use Domain\InterfaceAdapter\Presenter\Security\UserPresenter;
use Sebk\SmallOrmForms\Form\Field;
use Sebk\SmallOrmForms\Form\OnFlyForm;
use Sebk\SmallOrmForms\Type\BoolType;
use Sebk\SmallOrmForms\Type\StringType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[asController]
class UserController extends AbstractController
{

    /**
     * @param CreateUserUseCase $createUserUseCase
     * @param Request $request
     * @return JsonResponse
     * @throws \Sebk\SmallOrmForms\Form\FieldException
     * @throws \Sebk\SmallOrmForms\Form\FieldNotFoundException
     * @throws \Sebk\SmallOrmForms\Type\TypeNotFoundException
     */
    #[Route('/users', name: 'userCreate', methods: ['POST'])]
    public function createUser(CreateUserUseCase $createUserUseCase, Request $request): JsonResponse
    {

        $form = (new OnFlyForm())
            ->addField('nickname', 'nickname', null, new StringType(), Field::MANDATORY)
            ->addField('email', 'email', null, new StringType(), Field::MANDATORY)
            ->addField('password', 'password', null, new StringType(), Field::MANDATORY)
            ->addField('role', 'role', null, new StringType(), Field::MANDATORY)
            ->fillFromStdClass(json_decode($request->getContent()))
        ;

        $messages = $form->validate();
        if (count($messages) != 0) {
            throw new BadRequestException(json_encode($messages));
        }

        $this->denyAccessUnlessGranted(
            UserVoter::CREATE,
            $request = new CreateUserRequest(
                $form->getValue('nickname'),
                $form->getValue('password'),
                $form->getValue('email'),
                $form->getValue('role'),
            )
        );

        $response = $createUserUseCase
            ->execute($request);

        return new JsonResponse(
            (new UserPresenter($response->getUser(), $response->getPasswordStrengthLevel()))
                ->toArray()
        );

    }


    /**
     * @param int $id
     * @param UpdateUserUseCase $updateUserUseCase
     * @param Request $request
     * @return JsonResponse
     * @throws \Sebk\SmallOrmForms\Form\FieldException
     * @throws \Sebk\SmallOrmForms\Form\FieldNotFoundException
     * @throws \Sebk\SmallOrmForms\Type\TypeNotFoundException
     */
    #[Route('/users/{id}', name: 'userUpdate', methods: ['POST'])]
    public function updateUser(int $id, UpdateUserUseCase $updateUserUseCase, Request $request): JsonResponse
    {

        $form = (new OnFlyForm())
            ->addField('nickname', 'nickname', null, new StringType(), Field::OPTIONAL)
            ->addField('email', 'email', null, new StringType(), Field::OPTIONAL)
            ->addField('password', 'password', null, new StringType(), Field::OPTIONAL)
            ->addField('role', 'role', null, new StringType(), Field::OPTIONAL)
            ->addField('enabled', 'enabled', null, new BoolType(), Field::OPTIONAL)
            ->fillFromStdClass(json_decode($request->getContent()))
        ;

        $messages = $form->validate();
        if (count($messages) != 0) {
            throw new BadRequestException(json_encode($messages));
        }

        $this->denyAccessUnlessGranted(
            UserVoter::UPDATE,
            $request = new UpdateUserRequest(
                $id,
                $form->getValue('nickname'),
                $form->getValue('email'),
                $form->getValue('role'),
                $form->getValue('password'),
                $form->getValue('enabled'),
            )
        );

        $response = $updateUserUseCase
            ->execute($request);

        return new JsonResponse(
            (new UserPresenter($response->getUser(), $response->getPasswordStrengthLevel()))
                ->toArray()
        );

    }

    /**
     * @param ListUserUseCase $listUserUseCase
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/users', name: 'userList', methods: ['GET'])]
    public function listUsers(ListUserUseCase $listUserUseCase): JsonResponse
    {

        $this->denyAccessUnlessGranted(
            UserVoter::LIST,
        );

        $users = ($listUserUseCase->execute(new ListUserRequest()))
            ->getUsers()
        ;

        $response = [];
        foreach ($users as $user) {
            $response[] = (new UserPresenter($user))->toArray();
        }

        return new JsonResponse($response);

    }

    /**
     * @param GetUserUseCase $getUserUseCase
     * @return JsonResponse
     */
    #[Route('/users/myself', name: 'getMyUser', methods: ['GET'])]
    public function getMyUser(GetUserUseCase $getUserUseCase): JsonResponse
    {

        return new JsonResponse(
            (new UserPresenter(
                $getUserUseCase->execute(
                    new GetUserRequest($this->getUser()->getId())
                )->getUser()
            ))->toArray()
        );

    }

}