# Symfony Clean Architecture Skeleton

<img src="/imgcoverage-badge.png" width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="img/phpstan-badge.png" width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="img/tests-badge.png" width="30%">

## Purpose

This skeleton application provide base clean architecture for Symfony application.

The actual version of Symfony is 6.3

## install

```bash
bin/init
```

## serve

```bash
bin/console swoole:server:start
```

To check, use your favorite browser and go to "http://localhost" to see symfony demo page.

## Clean architecture architecture implementation

![Clean architecture cone image](https://framagit.org/small/symfony-clean-architecture/-/raw/master/img/clean-architecture-cone.jpeg)

The clean architectures intend to isolate business code that rarely change when technical code changing every day.

- The entities are business object 
  - They represent the data (at sens of business not ORM)
  - They implement business rules
- The use cases are the implementation of application business process and business rules
  - Use case classes
  - Request
  - Response
  - Request
- The interface adapters
  - controllers (http handling)
  - gateways (interfaces)
  - presenters (class that format the driver response)
  - managers (database and other )
- The end level are framework and drivers
  - input driver (http, message broker reception, websockets...)
  - output drivers (ORM implementation, message broker messages emission, other db drivers)
  - framework code

## Implementation

In this implementation, we have two main directories :
- "Domain" : all application and business specific rules code
- "Infrastructure" : all technical code attached on framework and device

You are free to structure this code according to external devices

The domain directory is structured subdirectory, representing your application :
- Interface adapters
  - directory "Gateway" contains all contracts (class interface)
  - directory "Controller" contains all input http managers
  - directory "Manager" contains all other managers (ORM managers, services to messages brokers...)
  - Presenter : classes that formalise a manager or a controller answer (for example html code)
- Applications business rules
  - Request : classes that formalise requesting data for a use case
  - Response : classes that formalise answer data of use case
  - UseCase : classes that implements application business rules
- Entity
  - Classes that implements the business rules and data organization